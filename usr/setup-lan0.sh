#!/bin/sh
#

[ -r ${APP_ROOT:-/opt/web-gui}/usr/functions.sh ] && . ${APP_ROOT:-/opt/web-gui}/usr/functions.sh

WAN_IP=`get_default_addr bond0`
LAN_IP=`get_next_network $WAN_IP`
CUR_IP=`get_default_addr lan0`
if [ "${CUR_IP}" != "${LAN_IP}" ]; then 
	logger -t $0 "Change ip address of lan0 from ${CUR_IP} to ${LAN_IP}"
	ip addr flush dev lan0
	ip addr add ${LAN_IP} dev lan0
	IP=`echo ${LAN_IP} | sed 's:/.*::'`
	sed -e "s/^dhcp-range=.*/dhcp-range=${IP}1,${IP}00,720h/" -i /etc/dnsmasq.conf
	service dnsmasq restart
else
	logger -t $0 "Address of lan0 ${CUR_IP} meets requirements"
	exit 0
fi

#!/bin/sh

[ -r /etc/conf.d/webgui ] && . /etc/conf.d/webgui

APP_ROOT=${APP_ROOT:-/opt/web-gui}

[ -r $APP_ROOT/usr/functions.sh ] && . $APP_ROOT/usr/functions.sh

R='\033[0;31m'
G='\033[0;32m'
Y='\033[0;33m'
N='\033[0m' # No Color
Line='-------------------------------------------------------'

show() {
    eval C=\$$1
    echo -e "${C}${2}${N}"
}

show G ${Line}

if [ "$1" = "-f" ] ; then 
    enable_repo
    install_software
    setup_lanif
    setup_dnsmasq
    enable_routing
    enable_masquerade
    # install_miniupnpd
    setup_fancontrol
else 
    echo Step by Step guide:
    show G ${Line}
    echo enable_repo 
    echo install_software
    echo setup_lanif
    echo setup_dnsmasq
    echo enable_routing
    echo enable_masquerade
    echo setup_fancontrol
    echo install_miniupnpd
fi

echo
show G ${Line}
show Y "Deploy OpenVPN keys into /etc/openvpn and /etc/wireguard while you have internet"
show R "Then reboot"

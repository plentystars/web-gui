#!/bin/sh

# Local variables in /etc/conf.d/webgui file

[ -r /etc/conf.d/webgui ] && . /etc/conf.d/webgui

LANIF=${LANIF:-eth0}
LANNAME=${LANNAME:-lan0}
TUNIF=${TUNIF:-tun0}
WLANIF=${WLANIF:-wlan0}
LANNET=${LANNET:-192.168.0}
APP_ROOT=${APP_ROOT:-/opt/web-gui}

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
NC='\033[0m' # No Color

iface_exists() {
        test -e "$ROOT"/sys/class/net/$1
}

setup_lanif(){

    echo -e "${Green}Execute ${FUNCNAME}${NC}"

    if iface_exists $LANIF; then 
	LANMAC=`cat /sys/class/net/eth0/address`
    elif iface_exists $LANNAME; then
	LANMAC=`cat /sys/class/net/lan0/address`
    fi

if [ -n ${LANMAC} ]; then

cat << EOF > /etc/network/interfaces
# --- WEBGUI MANAGEMENT ---
auto lo
iface lo inet loopback

auto ${LANNAME}
iface ${LANNAME} inet static
    address ${LANNET}.1/24
    pre-up /sbin/nameif -s ${LANNAME} ${LANMAC}

auto d-link
iface d-link inet manual
   pre-up /sbin/nameif -s d-link bc:22:28:fc:43:dd

# auto ${WLANIF}
# iface ${WLANIF} inet dhcp

auto bond0
iface bond0 inet dhcp
        bond-slaves d-link ${WLANIF} # order matter
        bond-mode active-backup
        bond-miimon 100
        bond-primary d-link
	post-up ${APP_ROOT}/usr/setup-lan0.sh
EOF
cat /etc/network/interfaces
else
	echo "Can't find MAC address for LAN"
fi
}

enable_repo(){
   
    echo -e "${Green}Execute ${FUNCNAME}${NC}"
    # Enable community repo 
    repo=http://dl-cdn.alpinelinux.org/alpine/latest-stable/community
    sed -e "s|#${repo}|${repo}|" -i /etc/apk/repositories
}

install_software(){
    
    echo -e "${Green}Execute ${FUNCNAME}${NC}"
    apk add --update util-linux bash sudo curl \
        dmidecode git patch vim mc python3 py3-pip bonding bridge \
        wireless-tools wpa_supplicant dnsmasq iptables openvpn \
	wireguard-tools wireguard-tools-wg-quick

    if [ ! -d ${APP_ROOT} ]; then 
	cd ${APP_ROOT}
	cd ..
        git clone https://gitlab.com/plentystars/web-gui.git
        cd web-gui
    else 
        cd ${APP_ROOT}
        git pull
    fi
    
    pip install -r requirements.txt
    cp etc/init.d/* /etc/init.d/
    if [ ! -f /etc/conf.d/webgui ]; then  
    	cp etc/conf.d/webgui /etc/conf.d/webgui
    else
	echo -e "${Green}/etc/conf.d/webgui exists, skipping${NC}"
    fi
    rc-update --quiet add webgui default
    service --quiet webgui start

    # TODO - Deploy keys
    # rc-update --quiet add openvpn default
    
}

setup_dnsmasq(){
    
echo -e "${Green}Execute ${FUNCNAME}${NC}"
# Enable dhcp on LANNAME
cat << EOF > /etc/dnsmasq.conf
interface=${LANNAME}
dhcp-range=${LANNET}.11,${LANNET}.99,720h
# dhcp-option=6,1.1.1.1,208.67.222.222,8.8.8.8
resolv-file=/etc/resolv.dnsmasq.conf
EOF
cp etc/resolv.dnsmasq.conf /etc/resolv.dnsmasq.conf
rc-update --quiet add dnsmasq default
}

enable_routing(){

    echo -e "${Green}Execute ${FUNCNAME}${NC}"
    echo 'net.ipv4.ip_forward=1' > /etc/sysctl.d/enable_ipv4_forward.conf
    echo 'net.ipv6.conf.all.disable_ipv6 = 1' > /etc/sysctl.d/disable_ipv6.conf
    sysctl -p
}

enable_masquerade(){

    echo -e "${Green}Execute ${FUNCNAME}${NC}"
    # Enable MASQUERADE on TUNIF
    patch -f -i usr/iptables.patch /etc/init.d/iptables
    iptables -t nat -F # May be conflict with miniupnpd
    iptables -t nat -A POSTROUTING -o ${TUNIF} -j MASQUERADE
    /etc/init.d/iptables save
    rc-update --quiet add iptables boot
    rc-update --quiet add wpa_supplicant boot
}

install_miniupnpd(){

    echo -e "${Green}Execute ${FUNCNAME}${NC}"
    # TODO - Enable Network locations applianse
    apk add miniupnpd
    echo ARGS=\" -i ${LANNAME} -a ${LANNAME} -z TONK_Router\" > /etc/conf.d/miniupnpd
    rc-update --quiet add miniupnpd default
}

setup_fancontrol(){

echo -e "${Green}Execute ${FUNCNAME}${NC}"
# Enable smart cooler control for TONK boxes
dmidecode -t system | grep 'Product Name: SAISHIAT'
if [ $? -eq 0 ]; then 
    apk add lm-sensors lm-sensors-detect lm-sensors-fancontrol
    cp -f etc/init.d/fancontrol  /etc/init.d/fancontrol
    chmod 0755 /etc/init.d/fancontrol
    cp -f etc/fancontrol  /etc/fancontrol
    rc-update --quiet add fancontrol
fi
}

check_bonding(){
    grep -q ${WLANIF} /proc/net/bonding/bond0
    if [ $? -ne 0 ]; then
        INTERFACE=${WLANIF}
    else
        INTERFACE=bond0
    fi
    echo $INTERFACE
}

connect_wifi() {
if [ -n "$2" ] ; then 
    wpa_passphrase "${1}" "${2}" | sed -e '/#psk=/d' > /etc/wpa_supplicant/wpa_supplicant.conf
    service wpa_supplicant restart
    INTERFACE=`check_bonding`
    ifconfig ${INTERFACE} down
    ifconfig ${INTERFACE} up
    # udhcpc -bi ${INTERFACE}
fi
}


get_default_addr() {
        # check if dhcpcd is running
        if pidof dhcpcd > /dev/null && [ -f "$ROOT/var/lib/dhcpc/dhcpcd-$1.info" ]; then
                echo dhcp
        elif iface_exists $1; then
                #$MOCK ip addr show $1 | awk '/inet / {print $2}' | head -n 1 | sed 's:/.*::'
                $MOCK ip addr show $1 | awk '/inet / {print $2}' | head -n 1 
        fi
}

get_default_mask() {
        if [ "$1" ] ; then
                ipcalc -m $1 | sed 's/.*=//'
        else
                echo "255.255.255.0"
        fi
}

get_default_bcast() {
        if [ "$1" ] ; then
                ipcalc -b $1 | sed 's/.*=//'
        else
                echo "192.168.250.255"
        fi
}

get_default_net() {
        if [ "$1" ] ; then
                ipcalc -n $1 | sed 's/.*=//'
        else
                echo "0.0.0.0"
        fi
}

get_default_gateway() {
        if iface_exists $1; then
                $MOCK ip route show dev $1 | awk '/^default/ {print $3}'
        fi
}

get_next_network(){
	IP_WITH_MASK=$1
	IFACENET=`get_default_net $IP_WITH_MASK`
	if [ "$IFACENET" != '192.168.0.0' ]; then
		NEXT_NET="192.168.0.1/24"
	else
		BROADCAST=`get_default_bcast $IP_WITH_MASK`
		BROADCAST_INT=`ip2dec $BROADCAST`
		NEXT_NET=`dec2ip $(($BROADCAST_INT+2))`
		NEXT_NET=${NEXT_NET}/24
	fi
	echo $NEXT_NET
}

ip2dec(){ 
	# Convert an IPv4 IP number to its decimal equivalent.
	echo $1 | sed 's:/.*::' | tr . '\n' | awk '{s = s*256 + $1} END {print s}'
}

dec2ip(){ 
	# Convert an IPv4 decimal IP value to an IPv4 IP.
	local ip delim dec=$1
        for e in 3 2 1 0
        do
    	    p256=$((256**$e))
    	    octet=$(($dec/256**$e))
    	    dec=$(($dec-$octet*256**$e))
            ip=${ip}${delim}${octet}
            delim=.
        done
        printf '%s\n' "$ip"
}



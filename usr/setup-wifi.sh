#!/bin/sh
#

[ -z "${2}" ] && exit 1

[ -r /etc/conf.d/webgui ] && . /etc/conf.d/webgui

APP_ROOT=${APP_ROOT:-/opt/web-gui}

[ -r $APP_ROOT/usr/functions.sh ] && . $APP_ROOT/usr/functions.sh

connect_wifi "$1" "$2"


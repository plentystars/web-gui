function connectvpn(data) {
  const url = '/connectvpn'

  const params = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  }
  
  return fetch(url, params)
}

function disconnectvpn() {
  const url = '/disconnectvpn'
  const params = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  }
  
  return fetch(url, params)
}



async function changeToDisconnectVPN() {
  const name = document.querySelector(`.table-item:nth-of-type(${ROW_ID}) td[data-type="Name"]`).innerText
  const type = 'openvpn'
  const data = { name, type }

  // Заблокировать экран (показать loader)
  document.querySelector('.disable-area').classList.remove('hidden')
  console.log(data)

  // Отправляем запрос
  try {
    const response = await connectvpn(data)
    // const data = await response.json()

    if (response.status === 200) {
      location.href = location.href
    }
  } catch (error) {
    console.log(err)
  }
}

async function changeToDisconnectWG() {
  const name = document.querySelector(`.table-item:nth-of-type(${ROW_ID}) td[data-type="Name"]`).innerText
  const type = 'wireguard'
  const data = { name, type }

  // Заблокировать экран (показать loader)
  document.querySelector('.disable-area').classList.remove('hidden')
  console.log(data)

  // Отправляем запрос
  try {
    const response = await connectvpn(data)
    // const data = await response.json()

    if (response.status === 200) {
      location.href = location.href
    }
  } catch (error) {
    console.log(err)
  }
}



async function changeToConnectVPN() {
  const name = document.querySelector(`.table-item:nth-of-type(${ROW_ID}) td[data-type="Name"]`).innerText
  const type = 'openvpn'
  const data = { name, type }

  // Заблокировать экран (показать loader)
  document.querySelector('.disable-area').classList.remove('hidden')
  console.log(data)
  
  // Отправляем запрос
  try {
    const response = await disconnectvpn(data)
    // const data = await response.json()

    if (response.status === 200) {
      location.href = location.href
    }
  } catch (error) {
    console.log(err)
  }
}

async function changeToConnectWG() {
  const name = document.querySelector(`.table-item:nth-of-type(${ROW_ID}) td[data-type="Name"]`).innerText
  const type = 'wireguard'
  const data = { name, type }

  // Заблокировать экран (показать loader)
  document.querySelector('.disable-area').classList.remove('hidden')
  console.log(data)
  
  // Отправляем запрос
  try {
    const response = await disconnectvpn(data)
    // const data = await response.json()

    if (response.status === 200) {
      location.href = location.href
    }
  } catch (error) {
    console.log(err)
  }
}

document.querySelectorAll(`.connect-vpn-btn`)
  .forEach(button => button.addEventListener('click', openModal))

function openModal(event) {
  const name = event.target.dataset.item

  // Заменяем надпись в модальном окне
  document.querySelector(`#connect-vpn-modal .modal-message span b`).innerHTML =  document.querySelector(`#connect-vpn-modal .modal-message span b`).innerText = name
}

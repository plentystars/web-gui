const tableRows = document.querySelectorAll('tr.table-item')

const rows = []
let ROW_ID = 0

// Формируем список строк таблицы
tableRows.forEach((row, index) => {
  // Массив td
  const children = row.children
  const obj = { id: index }
  
  for (let i = 0; i < children.length; i++) {
    const [key] = Object.values(children[i].dataset)

    if (key) {
      obj[key] = children[i].innerText
    }
  }

  rows.push(obj)
})

// Вешаем событие ввода текста в поиск
document.querySelector('.finder').addEventListener('input', (event) => {
  let content = event.target.value
  
  if (!content) redrawTable(rows)

  const result = rows.filter(row => {
    let flag = false
    
    let fields = Object.keys(row)
    fields = fields.filter(field => !['id'].includes(field))
    
    fields.forEach(field => {
      const str = String(row[field]).toLocaleLowerCase()
      content = content.toLocaleLowerCase() 

      if (str.includes(content)){
        flag = true
      }
    })

    return flag
  })

  redrawTable(result)
})

// const inputs = document.querySelectorAll('td input')

// // Вешаем событие ввода текста во все input
// for (let i = 0; i < inputs.length; i++) {
//   const input = inputs[i]

//   input.addEventListener('input', (event) => {
//     const inputValues = getInputValues()
//     const fields = Object.keys(inputValues)
//     const values = Object.values(inputValues)

//     if (values.every(value => value === '')) {
//       return redrawTable(rows)
//     }

//     const result = rows.filter(row => {
//       let flag = false
      
//       fields.forEach(field => {
//         if (row[field].includes(inputValues[field]) && inputValues[field] !== '') 
//           flag = true
//       })

//       return flag
//     })

//     redrawTable(result)
//   })
// }

// function getInputValues() {
//   const inputs = document.querySelectorAll('td input')

//   const obj = {}

//   for (let i = 0; i < inputs.length; i++) {
//     const id = inputs[i].id
  
//     obj[id] = inputs[i].value
//   }

//   return obj
// }

function redrawTable(rows) {
  // Скрывает все элементы
  const elements = document.querySelectorAll('.table-item')
  elements.forEach(item => item.classList.add('hidden'))

  // Заново показывает все элементы таблицы
  rows.forEach(({ id }) => {
    document.querySelectorAll('.table-item')[id].classList.remove('hidden')
  })

  // for (let i = 0 i < rows.length i++) {
  //   const row = rows[i]
  
  //   document.querySelector('table.table tbody')
  //     .append(createItem(row))
  // }
}

// function createItem(row) {
//   const tr = document.createElement('tr')
//   tr.classList.add('table-item')

//   const keys = Object.keys(row)

//   keys.forEach(key => {
//     const td = document.createElement('td')
//     td.setAttribute('data-type', key)
//     td.innerText = row[key]

//     tr.append(td)
//   })

//   const td = document.createElement('td')
  
//   const button = document.createElement('button')
//   button.classList.add('btn', 'btn-success')
//   button.innerText = 'Connect'
  
//   td.append(button)

//   tr.append(td)

//   return tr
// }

function connection(data) {
  const url = '/connect'

  const params = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  }
  
  return fetch(url, params)
}

function disconnection() {
  const url = '/disconnect'
  const params = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    }
  }
  
  return fetch(url, params)
}

async function changeToDisconnect() {
  const name = document.querySelector(`.table-item:nth-of-type(${ROW_ID}) td[data-type="Name"]`).innerText
  const password = document.querySelector('.modal-password-input').value

  const data = { name, password }

  // Заблокировать экран (показать loader)
  document.querySelector('.disable-area').classList.remove('hidden')

  // Отправляем запрос
  try {
    const response = await connection(data)
    // const data = await response.json()

    if (response.status === 200) {
      location.href = location.href
    }
  } catch (error) {
    console.log(err)
  }
}

async function changeToConnect() {
  // Заблокировать экран (показать loader)
  document.querySelector('.disable-area').classList.remove('hidden')
  
  // Отправляем запрос
  try {
    const response = await disconnection()
    // const data = await response.json()

    if (response.status === 200) {
      location.href = location.href
    }
  } catch (error) {
    console.log(err)
  }
}

// Вешаем событие щелчка на все кнопки внутри таблицы
const buttons = document.querySelectorAll('td button')
buttons.forEach(button => {
  button.addEventListener('click', disconnectToggle)
})

function disconnectToggle(event) {
  ROW_ID = Number(event.target.dataset.id)
}

// Програмируем глазик
document.querySelector('.password-eye')
  .addEventListener('click', (event) => {
    const status = Boolean(Number(event.target.dataset.active))

    if (status) {
      // Заменяем картинку
      event.target.src = event.target.src.replace('eye.png', 'no-eye.png')

      // Меняем статус
      event.target.dataset.active = '0'

      // Меняем тип imput
      document.querySelector('.modal-password-input').type = 'password'
    } else {
      // Заменяем картинку
      event.target.src = event.target.src.replace('no-eye.png', 'eye.png')

      // Меняем статус
      event.target.dataset.active = '1'

      // Меняем тип input
      document.querySelector('.modal-password-input').type = 'text'
    }
  })

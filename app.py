#!/usr/bin/env python3

import sys
import subprocess
import urllib
import json
import os
from flask import Flask, render_template, send_from_directory, request, jsonify, redirect, url_for
import re
from syslog import syslog

debug = True
wlaninterface = os.environ.get("WLANIF", 'wlan0')
bondinterface = "bond0"


def log(msg):
    msg = '<web-gui>: ' + str(msg)
    syslog(msg)
    if debug:
        print(msg)


# You can add or change the functions to parse the properties of each AP (cell)
# below. They take one argument, the bunch of text describing one cell in iwlist
# scan and return a property of that cell.


def get_address(cell):
    return matching_line(cell, "Address: ")


def get_name(cell):
    return matching_line(cell, "ESSID:")[1:-1]


def get_protocol(cell):
    if matching_line(cell, "Protocol:"):
        return matching_line(cell, "Protocol:").split()[1]
    else:
        return '802.11'


def get_quality(cell):
    if matching_line(cell, "Quality="):
        quality = matching_line(cell, "Quality=").split()[0].split('/')
        return str(int(round(float(quality[0]) / float(quality[1]) * 100))).rjust(3) + " %"
    else:
        return 'Unknown'


def get_channel(cell):
    if matching_line(cell, "Channel:"):
        return matching_line(cell, "Channel:")
    else:
        return 'Unknown'


def get_signal_level(cell):
    if matching_line(cell, "Quality="):
        # Signal level is on same line as Quality data so a bit of ugly
        # hacking needed...
        return matching_line(cell, "Quality=").split("Signal level=")[1]
    else:
        return 'Unknown'


def get_encryption(cell):
    enc = ""
    if matching_line(cell, "Encryption key:") == "off":
        enc = "Open"
    else:
        for line in cell:
            matching = match(line, "IE:")
            if matching != None:
                wpa = match(matching, "WPA Version ")
                if wpa != None:
                    enc = "WPA v."+wpa
        if enc == "":
            enc = "WEP"
    return enc


def matching_line(lines, keyword):
    """Returns the first matching line in a list of lines. See match()"""
    for line in lines:
        matching = match(line, keyword)
        if matching != None:
            return matching
    return None


def match(line, keyword):
    """If the first part of line (modulo blanks) matches keyword,
    returns the end of that line. Otherwise returns None"""
    line = line.lstrip()
    length = len(keyword)
    if line[:length] == keyword:
        return line[length:]
    else:
        return None


def parse_cell(cell):
    """Applies the rules to the bunch of text describing a cell and returns the
    corresponding dictionary"""
    parsed_cell = {}
    for key in rules:
        rule = rules[key]
        parsed_cell.update({key: rule(cell)})
    return parsed_cell


def get_main_ip():
    import socket
    local_ip = ''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(("8.8.8.8", 80))
        local_ip = s.getsockname()[0]
        s.close()
    except socket.error as e:
        pass
    log("localip = %s" % local_ip)
    return local_ip


def get_wifi_list():
    """Pretty prints the output of iwlist scan into a table"""

    cells = [[]]
    parsed_cells = []

    proc = subprocess.Popen(["ifconfig", wlaninterface, "up"],
                            stdout=subprocess.PIPE, universal_newlines=True)
    proc = subprocess.Popen(["iwlist", wlaninterface, "scan"],
                            stdout=subprocess.PIPE, universal_newlines=True)
    out, err = proc.communicate()

    for line in out.split("\n"):
        cell_line = match(line, "Cell ")
        if cell_line != None:
            cells.append([])
            line = cell_line[-27:]
        cells[-1].append(line.rstrip())

    cells = cells[1:]

    for cell in cells:
        parsed_cells.append(parse_cell(cell))

    parsed_cells = sorted(
        parsed_cells, key=lambda d: d['Quality'], reverse=True)
    return parsed_cells


def get_wifi_status():
    wifi = {}
    proc = subprocess.Popen(
        ["iwgetid", "-r", wlaninterface], stdout=subprocess.PIPE, universal_newlines=True)
    out, err = proc.communicate()
    ssid_name = out.strip()
    if proc.returncode == 0 and ssid_name:
        wifi["name"] = ssid_name
        wifi["status"] = 'Connected'
    log(wifi)
    return wifi


def get_vpn_status():
    vpn = {}
    main_ip = get_main_ip()
    if main_ip.startswith('10.'):
        vpn["status"] = "Connected"
        vpn["ip"] = main_ip
    log(vpn)
    return vpn


def files(path):
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)) and re.match(r'.*\.conf', file):
            yield file

# Here's a dictionary of rules that will be applied to the description of each
# cell. The key will be the name of the column in the table. The value is a
# function defined above.


rules = {"Name": get_name, "Quality": get_quality, "Channel": get_channel, "Encryption": get_encryption,
         "Address": get_address, "Protocol": get_protocol, "Signal": get_signal_level}

app = Flask(__name__)


@app.route('/<path:path>')
def send_report(path):
    return send_from_directory('static', path)


@app.route('/')
def status():
    data = {}
    edns = {}
    wifi_status = get_wifi_status()
    vpn_status = get_vpn_status()
    try:
        with urllib.request.urlopen("http://ip-api.com/json/", timeout=5) as url:
            data = json.load(url)
    except urllib.error.URLError as e:
        pass
    try:
        with urllib.request.urlopen("http://edns.ip-api.com/json/", timeout=5) as url:
            edns = json.load(url)
    except urllib.error.URLError as e:
        pass

    data["wifi_name"] = wifi_status.get("name")
    data["wifi_status"] = wifi_status.get("status")
    data["vpn_ip"] = vpn_status.get("ip")
    data["vpn_status"] = vpn_status.get("status")
    data["dns"] = edns.get("dns")
    data["edns"] = edns.get("edns")
    log(data)
    return render_template('status.html', js_var=data)


@app.route('/wifi')
def wifi():
    wifi_list = get_wifi_list()
    wifi_status = get_wifi_status()
    for cell in wifi_list:
        if cell["Name"] == wifi_status.get("name"):
            cell["Status"] = 'Connected'
    log(wifi_list)
    return render_template('wifi.html', js_var=wifi_list)


@app.route('/dns')
def dns():
    return render_template('dns.html')


@app.route('/bond')
def bond():
    status = {}
    procfile = "/proc/net/bonding/%s" % bondinterface
    if os.path.isfile(procfile):
        with open(procfile, 'r') as file:
            status["body"] = file.read()
            status["bond"] = True

    else:
        status["body"] = 'No bonding'
    return render_template('bond.html', js_var=status)


@app.route('/openvpn')
def openvpn():
    path = "/etc/openvpn"
    configs = []
    link = ''
    for file in files(path):
        if os.path.islink(os.path.join(path, file)):
            link = os.readlink(os.path.join(path, file))
        else:
            configs.append({'Name': file, 'Type': 'openvpn'})

    for config in configs:
        if config['Name'] == link:
            config['active'] = True

    return render_template('openvpn.html', js_var=configs)


@app.route('/wireguard')
def wireguard():
    path = "/etc/wireguard"
    configs = []
    link = ''
    for file in files(path):
        if os.path.islink(os.path.join(path, file)):
            link = os.readlink(os.path.join(path, file))
        else:
            configs.append({'Name': file, 'Type': 'wireguard'})

    for config in configs:
        if config['Name'] == link:
            config['active'] = True

    return render_template('wireguard.html', js_var=configs)


@app.route('/connect', methods=['POST', 'GET'])
def wifi_connect():
    if request.method == 'POST':
        args = request.form
        command = "/opt/web-gui/usr/setup-wifi.sh '%s' '%s'" % (
            request.json.get("name"), request.json.get("password"))
        log(command)
        os.system(command)
        wifi_list = get_wifi_list()
        wifi_status = get_wifi_status()
        return redirect(url_for('wifi'))


@app.route('/disconnect', methods=['POST', 'GET'])
def wifi_disconnect():
    proc = subprocess.Popen(["ifconfig", wlaninterface, "down"],
                            stdout=subprocess.PIPE, universal_newlines=True)
    wifi_list = get_wifi_list()
    wifi_status = get_wifi_status()
    return render_template('wifi.html', js_var=wifi_list)


@app.route('/connectvpn', methods=['POST', 'GET'])
def vpn_connect():
    if request.method == 'POST':
        args = request.form

        if request.json.get("type") == 'openvpn':
            command = "cd /etc/openvpn ; ln -sf %s openvpn.conf ; service wireguard stop ; rc-update del wireguard ; rm -f /etc/wireguard/wg0.conf ; rc-update add openvpn ; service openvpn restart" % (
                request.json.get("name"))
        if request.json.get("type") == 'wireguard':
            command = "cd /etc/wireguard ; ln -sf %s wg0.conf ; service openvpn stop ; rc-update del openvpn ; rm -f /etc/openvpn/openvpn.conf ; rc-update add wireguard ; service wireguard restart" % (
                request.json.get("name"))

        os.system(command)
        return redirect(url_for(request.json.get("type")))


@app.route('/update', methods=['POST', 'GET'])
def update():
    command = "cd /opt/web-gui ; git pull"
    log(command)
    os.system(command)
    return redirect('/')


if __name__ == '__main__':
    app.run(debug=debug, host='0.0.0.0', port=80)
    # app.run(debug=debug, host='0.0.0.0')
